package com.itstyle.quartz.job;

import java.io.Serializable;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

public class ChickenJob implements Job, Serializable {

	private static final long serialVersionUID = 1L;


	
	@Autowired
    private StringRedisTemplate stringRedisTemplate;
     
    @Autowired
    private RedisTemplate redisTemplate;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		stringRedisTemplate.opsForValue().set("aaa", "111");
		System.out.println(stringRedisTemplate.opsForValue().get("aaa"));
		System.out.println("大吉大利、今晚吃鸡");
	}
}
