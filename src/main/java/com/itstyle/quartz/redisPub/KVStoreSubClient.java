package com.itstyle.quartz.redisPub;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import redis.clients.jedis.JedisPubSub;

import com.itstyle.quartz.Application;
import com.itstyle.quartz.config.RedisMessageListener;


@Component
public class KVStoreSubClient extends Thread {
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
	private RedisMessageListenerContainer container;
	
	@Autowired
	RedisMessageListener redisMessageListener;
	
	
	public void setChannelAndListener(String channel){
		//container.
    }
	
	/**
	 * 订阅一个频道
	 */
	   private void subscribe(){
		    String channel="zhang";
	        System.out.println("  >>> 订阅(SUBSCRIBE) > Channel:"+channel);
	        
	        System.out.println();
	        //接收者在侦听订阅的消息时，将会阻塞进程，直至接收到quit消息（被动方式），或主动取消订阅
	       // container.
	    }

}
