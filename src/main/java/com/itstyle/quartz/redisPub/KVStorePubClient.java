package com.itstyle.quartz.redisPub;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import com.itstyle.quartz.Application;
import com.itstyle.quartz.config.RedisMessageListener;

/**
 * 消息发布与订阅
 * 
 * @author zhangwenzhi
 *
 */
@Service
public class KVStorePubClient {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	RedisMessageListenerContainer redisMessageListenerContainer;

	@Autowired
	RedisMessageListener redisMessageListener;

	/**
	 * 消息发布者 发布频道与消息
	 * 
	 * @param channel
	 * @param message
	 */
	public void pub(String channel, String message) {
		System.out.println("  >>> 发布(PUBLISH) > Channel:" + channel
				+ " > 发送出的Message:" + message);

		redisMessageListenerContainer.addMessageListener(redisMessageListener,
				new PatternTopic(channel));

		// redisTemplate.convertAndSend(channel, message);
	}

	/**
	 * 关闭濒道
	 * 
	 * @param channel
	 */
	public void close(String channel) {
		redisMessageListenerContainer.removeMessageListener(
				redisMessageListener, new PatternTopic(channel));
		// redisMessageListenerContainer.removeMessageListener(redisMessageListener);

	}

}
