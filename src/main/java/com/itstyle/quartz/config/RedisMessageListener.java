package com.itstyle.quartz.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Component
public class RedisMessageListener implements MessageListener{
	
	@Autowired
	private RedisTemplate redisTemplate;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		
		byte[] body = message.getBody();
		
		String msgBody = (String) redisTemplate.getValueSerializer().deserialize(body);
		System.out.println("message:"+msgBody+"===========================");
		byte[] channel = message.getChannel();
		String msgChannel = (String)redisTemplate.getValueSerializer().deserialize(channel);
		System.out.println("msgChannel:"+msgChannel+"-----------------------------");
		String msgPattern = new String(pattern);
		System.out.println("msgPattern"+msgPattern+"8888888888");

	}

}
