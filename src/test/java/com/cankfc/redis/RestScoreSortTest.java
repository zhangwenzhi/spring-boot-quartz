package com.cankfc.redis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.test.context.junit4.SpringRunner;

import com.itstyle.quartz.Application;

/**
 * 最佳实践
 * 
 * https://help.aliyun.com/document_detail/26366.html?spm=a2c4g.11186623.6.608
 * .26825b12r1q7HM
 * 
 * @author zhangwenzhi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestScoreSortTest {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	private static int TOTAL_SIZE = 20;// 玩家人数

	/**
	 * 游戏玩家积分排行榜
	 * 
	 * @throws Exception
	 */
	@Test
	public void ScoreSortTest() throws Exception {
		String key = "Glory of the king";// 王者荣耀
		ZSetOperations<String, Object> zSetOps = redisTemplate.opsForZSet();
		/**
		 * //清除可能的已有数据
		 */
		redisTemplate.delete(key);//

		// 模拟生成若干个游戏玩家
		List<String> playerList = new ArrayList<String>();
		for (int i = 0; i < TOTAL_SIZE; ++i) {
			// 随机生成每个玩家的ID
			playerList.add(UUID.randomUUID().toString());
		}
		System.out.println("输入所有玩家 ");

		// 记录每个玩家的得分
		for (int i = 0; i < playerList.size(); i++) {
			// 随机生成数字，模拟玩家的游戏得分
			int score = (int) (Math.random() * 5000);
			String member = playerList.get(i);
			System.out.println("玩家ID：" + member + "， 玩家得分: " + score);
			// 将玩家的ID和得分，都加到对应key的SortedSet中去
			zSetOps.add(key, member, score);
		}

		// 输出打印全部玩家排行榜
		System.out.println();
		System.out.println("       " + key);
		System.out.println("       全部玩家排行榜 ");
		// 从对应key的SortedSet中获取已经排好序的玩家列表
		Cursor<TypedTuple<Object>> cursor = zSetOps.scan(key, ScanOptions.NONE);

		while (cursor.hasNext()) {// 遍历所有的值
			TypedTuple<Object> item = cursor.next();
			System.out.println("玩家ID：" + item.getValue() + "， 玩家得分:"
					+ Double.valueOf(item.getScore()).intValue());
		}

		// 输出打印Top5玩家排行榜
		System.out.println();
		System.out.println("       " + key);
		System.out.println("      前五名 Top 玩家");

		// Set<Object> set = zSetOps.range(key, 0, 4);
		// zSetOps.reverseRangeByScoreWithScores(key, min, max, offset, count)
		
		long count =zSetOps.zCard(key);//集合中元素的数量
		System.out.print(count);
		Set<TypedTuple<Object>> set = zSetOps.rangeWithScores(key, TOTAL_SIZE-4, TOTAL_SIZE-1);
		Iterator<TypedTuple<Object>> it = set.iterator();
		while (it.hasNext()) {
			TypedTuple<Object> t = it.next();
			System.out.println("玩家ID：" + t.getValue() + "， 玩家得分:"
					+ Double.valueOf(t.getScore()).intValue());
		}

	}

}
