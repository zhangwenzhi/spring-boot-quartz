package com.cankfc.redis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.test.context.junit4.SpringRunner;

import com.itstyle.quartz.Application;

/**
 * 网上商城商品相关性分析
 * 
 * @author zhangwenzhi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AliyunShoppingMall {

	@Autowired
	private RedisTemplate redisTemplate;

	/**
	 * 网上商城商品相关性分析
	 * 
	 * @throws Exception
	 */
	@Test
	public void ShoppingMallTest() throws Exception {

		ZSetOperations<String, Object> zSetOps = redisTemplate.opsForZSet();
		// 产品列表
		String key0 = "阿里云:产品:啤酒";
		String key1 = "阿里云:产品:巧克力";
		String key2 = "阿里云:产品:可乐";
		String key3 = "阿里云:产品:口香糖";
		String key4 = "阿里云:产品:牛肉干";
		String key5 = "阿里云:产品:鸡翅";
		// final String[] aliyunProducts=new
		// String[]{key0,key1,key2,key3,key4,key5};
		List<String> keyList = new ArrayList<String>();
		keyList.add(key0);
		keyList.add(key1);
		keyList.add(key2);
		keyList.add(key3);
		keyList.add(key4);
		keyList.add(key5);
		// 初始化，清除可能的已有旧数据
		redisTemplate.delete(keyList);

		// 模拟用户购物
		for (int i = 0; i < 5; i++) {// 模拟多人次的用户购买行为
			customersShopping(keyList, i, zSetOps);
		}

		// 利用 Redis来输出各个商品间的关联关系
		for (int i = 0; i < keyList.size(); i++) {
			System.out.println(">>>>>>>>>>与" + keyList.get(i)
					+ "一起被购买的产品有<<<<<<<<<<<<<<<");

			Cursor<TypedTuple<Object>> cursor = zSetOps.scan(keyList.get(i),
					ScanOptions.NONE);
			while (cursor.hasNext()) {
				TypedTuple<Object> item = cursor.next();
				System.out.println("商品名称：" + item.getValue() + "， 共同购买次数:"
						+ Double.valueOf(item.getScore()).intValue());
			}
			System.out.println();
		}

	}

	/**
	 * 模拟多人次的用户购买行为
	 * 
	 * @param keyList
	 * @param i
	 */
	private void customersShopping(List<String> keyList, int i,
			ZSetOperations<String, Object> zSetOps) {
		// 简单模拟3种购买行为，随机选取作为用户的购买选择
		int bought = (int) (Math.random() * 3);

		if (bought == 1) {
			// 模拟业务逻辑：用户购买了如下产品
			System.out.println("用户" + i + "购买了" + keyList.get(0) + ","
					+ keyList.get(1) + "," + keyList.get(2));
			zSetOps.incrementScore(keyList.get(0), keyList.get(1), 1);
			zSetOps.incrementScore(keyList.get(0), keyList.get(2), 1);
			zSetOps.incrementScore(keyList.get(1), keyList.get(0), 1);
			zSetOps.incrementScore(keyList.get(1), keyList.get(2), 1);
			zSetOps.incrementScore(keyList.get(2), keyList.get(0), 1);
			zSetOps.incrementScore(keyList.get(2), keyList.get(1), 1);
		} else if (bought == 2) {
			// 模拟业务逻辑：用户购买了如下产品
			System.out.println("用户" + i + "购买了" + keyList.get(4) + ","
					+ keyList.get(2) + "," + keyList.get(3));
			zSetOps.incrementScore(keyList.get(4), keyList.get(2), 1);
			zSetOps.incrementScore(keyList.get(4), keyList.get(3), 1);
			zSetOps.incrementScore(keyList.get(2), keyList.get(4), 1);
			zSetOps.incrementScore(keyList.get(2), keyList.get(3), 1);
			zSetOps.incrementScore(keyList.get(3), keyList.get(4), 1);
			zSetOps.incrementScore(keyList.get(3), keyList.get(2), 1);
		} else {
			// 模拟业务逻辑：用户购买了如下产品
			System.out.println("用户" + i + "购买了" + keyList.get(1) + ","
					+ keyList.get(5));
			zSetOps.incrementScore(keyList.get(1), keyList.get(5), 1);
			zSetOps.incrementScore(keyList.get(5), keyList.get(1), 1);

		}
	}
}
