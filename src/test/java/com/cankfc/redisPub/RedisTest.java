package com.cankfc.redisPub;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.itstyle.quartz.Application;
import com.itstyle.quartz.redisPub.KVStorePubClient;

/**
 * 最佳实践
 * 
 * https://help.aliyun.com/document_detail/26366.html?spm=a2c4g.11186623.6.608
 * .26825b12r1q7HM
 * 
 * @author zhangwenzhi
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RedisTest {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	
	@Autowired
	KVStorePubClient kVStorePubClient;

	private static int TOTAL_SIZE = 20;// 玩家人数

	/**
	 * 游戏玩家积分排行榜
	 * 
	 * @throws Exception
	 */
	@Test
	public void ScoreSortTest() throws Exception {
		
		kVStorePubClient.pub("zhang", "message");
		redisTemplate.convertAndSend("zhang", "zhangwenzhi----------------------");
	}

}
